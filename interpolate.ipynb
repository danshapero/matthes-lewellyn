{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib notebook\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "from matplotlib.tri import Triangulation\n",
    "import grid\n",
    "import firedrake\n",
    "import icepack.plot"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Reading and interpolating data\n",
    "\n",
    "Load in the GPS positions at the start and end of the survey, and the times where the positions were taken.\n",
    "Dividing the displacement by the time gives us the surface velocity."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X0, X1, dt = grid.load_velocity_data()\n",
    "dx = X1 - X0\n",
    "v = dx / dt * 365.25 * 24 * 3600"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tria = Triangulation(X0[0, :]/1e3, X0[1, :]/1e3)\n",
    "\n",
    "fig, axes = plt.subplots()\n",
    "axes.set_aspect('equal')\n",
    "speed = np.sqrt(v[0, :]**2 + v[1, :]**2)\n",
    "contours = axes.tricontourf(tria, speed, 20)\n",
    "axes.scatter(X0[0, :]/1e3, X0[1, :]/1e3, color='k', marker='.', alpha=0.25)\n",
    "colorbar = fig.colorbar(contours, label='m / yr')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Xs, ds, ss = grid.load_radar_data()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The choice of function determines how we're going to interpolate the radar data to a regular grid.\n",
    "Try changing it to, say, `'inverse'`, `'gaussian'`, `'linear'`, `'cubic'`, or `'quintic'`.\n",
    "The parameter $\\lambda$ dictates how smooth we want the solution to be.\n",
    "Try changing this to, say, 0.5 or 50."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "function = 'thin_plate'\n",
    "λ = 5\n",
    "\n",
    "itp_d, itp_s = grid.interpolate_radar_data(Xs, ds, ss, function='thin_plate', smooth=λ)\n",
    "d, s = itp_d(X0[0, :], X0[1, :]), itp_s(X0[0, :], X0[1, :])\n",
    "b = s - d"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following figures show the bed, surface, and thickness."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, axes = plt.subplots()\n",
    "axes.set_aspect('equal')\n",
    "contours = axes.tricontourf(tria, b, 20)\n",
    "fig.colorbar(contours)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, axes = plt.subplots()\n",
    "axes.set_aspect('equal')\n",
    "contours = axes.tricontourf(tria, s, 20)\n",
    "fig.colorbar(contours)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, axes = plt.subplots()\n",
    "axes.set_aspect('equal')\n",
    "contours = axes.tricontourf(tria, s - b, 20)\n",
    "fig.colorbar(contours)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Making a computational mesh\n",
    "\n",
    "To run our model, we need to first have a **triangulation** of the domain.\n",
    "Triangulations are nice because they can represent weirdly-shaped regions, like an icefield with loads of nunataks and peaks sticking out of it.\n",
    "To do this, we'll first extract the corners of the domain."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "left_index = np.argmin(X0[0, :])\n",
    "right_index = np.argmax(X0[0, :])\n",
    "bottom_index = np.argmin(X0[1, :])\n",
    "top_index = np.argmax(X0[1, :])\n",
    "\n",
    "left_point = X0[:, left_index]\n",
    "bottom_point = X0[:, bottom_index]\n",
    "top_point = X0[:, top_index]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once we have the corner points, we can pull out what we really need -- the length of each side and the angle to rotate everything by."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Lx = np.sqrt(sum((bottom_point - left_point)**2))\n",
    "Ly = np.sqrt(sum((top_point - left_point)**2))\n",
    "\n",
    "direction = top_point - left_point\n",
    "angle = np.arctan(direction[1] / direction[0])\n",
    "\n",
    "print(Lx, Ly)\n",
    "print(angle / np.pi)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Create a matrix that will apply a rotation by the angle we just found."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Q = np.array([[+np.cos(angle), +np.sin(angle)],\n",
    "              [-np.sin(angle), +np.cos(angle)]])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The finite element modeling package we'll use has a built-in function to make a rectangular mesh.\n",
    "(For oddly-shaped domains, we have to do a lot of this ourselves -- more on that later.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "nx, ny = 7, 13\n",
    "mesh = firedrake.RectangleMesh(nx, ny, Lx, Ly)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The mesh that we just created has the right size, but not the right position."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, axes = icepack.plot.subplots()\n",
    "icepack.plot.triplot(mesh, axes=axes)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To reposition the mesh, we'll pull out the array of vertex coordinates.\n",
    "We'll then rotate and translate every point to the position we want."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "coords = mesh.coordinates.dat.data\n",
    "for point in coords:\n",
    "    point[:] = (Q.dot(point) + left_point)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now everything's where it should be!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, axes = icepack.plot.subplots()\n",
    "icepack.plot.triplot(mesh, axes=axes)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Function spaces\n",
    "\n",
    "Once we have a mesh of our domain, we have to decide how we're going to represent fields like the ice thickness, velocity, etc.\n",
    "In our case, we'll make the simplest choice possible -- everything will be piecewise linear within each triangle.\n",
    "If we wanted to get fancy this could be piecewise quadratic."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Q = firedrake.FunctionSpace(mesh, family='CG', degree=1)\n",
    "V = firedrake.VectorFunctionSpace(mesh, family='CG', degree=1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "d = firedrake.Function(Q)\n",
    "s = firedrake.Function(Q)\n",
    "\n",
    "d.dat.data[:] = itp_d(coords[:, 0], coords[:, 1])\n",
    "s.dat.data[:] = itp_s(coords[:, 0], coords[:, 1])\n",
    "b = firedrake.interpolate(s - d, Q)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, axes = icepack.plot.subplots()\n",
    "contours = icepack.plot.tricontourf(b, axes=axes)\n",
    "fig.colorbar(contours)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, axes = icepack.plot.subplots()\n",
    "contours = icepack.plot.tricontourf(s, axes=axes)\n",
    "fig.colorbar(contours)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now I have to do something horrible to get the velocities right.\n",
    "I apologize."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "u = firedrake.Function(V)\n",
    "\n",
    "for input_index in range(nx * ny):\n",
    "    x_input = X0[:, input_index]\n",
    "    output_index = 0\n",
    "    min_dist = np.inf\n",
    "    for index in range(nx * ny):\n",
    "        x_output = coords[index, :]\n",
    "        dist = np.sqrt(sum((x_output - x_input)**2))\n",
    "        if dist < min_dist:\n",
    "            min_dist = dist\n",
    "            output_index = index\n",
    "            \n",
    "    u.dat.data[output_index, :] = v[:, input_index]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, axes = icepack.plot.subplots()\n",
    "arrows = icepack.plot.quiver(u)\n",
    "fig.colorbar(arrows)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "firedrake",
   "language": "python",
   "name": "firedrake"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}

from datetime import datetime, timedelta
import numpy as np
from scipy import interpolate
import matplotlib.pyplot as plt
import matplotlib.tri
import pandas
import pyproj

def parse_date_string(date_string):
    date, time = date_string.split(' ')
    month, day, year = date.split('/')
    year = int(year) if int(year) > 2000 else int(year) + 2000
    hour, minute = time.split(':')
    return datetime(year, int(month), int(day), int(hour), int(minute))


def load_velocity_data():
    filename = ('Data/2018/GPS_surface velocities/'
                'Matthes-Llewellyn Divide Movement Vectors.csv')
    raw_data = pandas.read_csv(filename)
    data = raw_data[raw_data['Easting(0)'].notnull()]

    x0, y0 = np.array(data['Easting(0)']), np.array(data['Northing(0)'])
    x1, y1 = np.array(data['Easting(1)']), np.array(data['Northing(1)'])

    times0 = np.array([parse_date_string(date_string)
                       for date_string in data['Date & Time(0)']])
    times1 = np.array([parse_date_string(date_string)
                       for date_string in data['Date & Time(1)']])
    dts = np.array([dt.total_seconds() for dt in (times1 - times0)])

    return np.array((x0, y0)), np.array((x1, y1)), dts


def load_radar_data():
    directory = 'Data/2018/Deep_radar/csvs/'
    filenames = ['mathes3processed_1.csv',
                 'mathes3processed_2.csv',
                 'mathes3processed_3.csv',
                 'mathes3processed_4.csv',
                 'mathes3processed_5.csv',
                 'mathesprocessed_1.csv',
                 'mathesprocessed_2.csv']
    Xs, ds, ss = [], [], []
    for filename in filenames:
        data = pandas.read_csv(directory + filename)
        lat, lon = np.array(data.iloc[:, 0]), np.array(data.iloc[:, 1])
        in_proj = pyproj.Proj(init='epsg:4326')
        out_proj = pyproj.Proj(init='epsg:32608')
        x, y = pyproj.transform(in_proj, out_proj, lon, lat)

        Xs.append(np.array((x, y)))
        ds.append(data.iloc[:, 2])
        ss.append(data['Elevation'])

    return Xs, ds, ss


def interpolate_radar_data(positions, depths, surfaces, **kwargs):
    X = np.hstack(positions)
    d = np.hstack(depths)
    s = np.hstack(surfaces)
    return (interpolate.Rbf(X[0, :], X[1, :], d, **kwargs),
            interpolate.Rbf(X[0, :], X[1, :], s, **kwargs))
